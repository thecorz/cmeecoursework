d <- read.table("../Data/SparrowSize.txt", header=TRUE)
str(d)
hist(d$Tarsus)
mean(d$Tarsus)
mean(d$Tarsus, na.rm = TRUE)
median(d$Tarsus, na.rm = TRUE)
mode(d$Tarsus)
par(mfrow = c(2,2))
hist(d$Tarsus, breaks = 3, col="grey")
hist(d$Tarsus, breaks = 10, col="grey")
hist(d$Tarsus, breaks = 30, col="grey")
hist(d$Tarsus, breaks = 100, col="grey")
require(modeest)
mlv(d$Tarsus)
d2 <- subset(d, d$Tarsus!="NA")
length(d$Tarsus)
length(d2$Tarsus)
mlv(d2$Tarsus)
range(d$Tarsus, na.rm = TRUE)
sum((d2$Tarsus - mean(d2$Tarsus))^2)/(length(d2$Tarsus)-1)
var(d2$Tarsus, na.rm = TRUE)
# Z-scores and quantiles
zTarsus <- (d2$Tarsus - mean(d2$Tarsus))/sd(d2$Tarsus)

#Exercise Calculate the mean variance and standat desviation of
#Bill length
#body mass
#wing lingth in r
#Plot al four histogram in a multi panel figure
mean(d$Bill, na.rm = TRUE)
var(d$Bill, na.rm = TRUE)
sd(d$Bill, na.rm = TRUE)

mean(d$Mass , na.rm = TRUE)
var(d$Mass, na.rm = TRUE)
sd(d$Mass, na.rm = TRUE)

mean(d$Wing, na.rm = TRUE)
var(d$Wing, na.rm = TRUE)
sd(d$Wing, na.rm = TRUE)

pdf("../Results/Histograms.HO2.2")
par(mfrow = c(2,2))
hist(d$Bill, breaks = 20)
hist(d$Mass, breaks = 20)
hist(d$Wing, breaks = 20)
hist(d$Tarsus, breaks = 20)
dev.off()

#z-scores and quantiles
zTarsus <- (d2$Tarsus - mean(d2$Tarsus))/sd(d2$Tarsus)
var(zTarsus)

#Normal distribution
set.seed(123)
znormal <- rnorm(1000000)
hist(znormal, breaks = 100)
qnorm(c(0.025, 0.975))
pnorm(.Last.value)

?boxplot
