#!/usr/bin/python
"""
Find Kingdom, Phylum and Species from the blackbirds file using
lookbehind
"""

import re

# Read the file
f = open('../Data/blackbirds.txt', 'r')
text = f.read()
f.close()

# remove \t\n and put a space in:
text = text.replace('\t',' ')
text = text.replace('\n',' ')

# note that there are "strange characters" (these are accents and
# non-ascii symbols) because we don't care for them, first transform
# to ASCII:
text = str(text.decode('ascii', 'ignore'))

# Now write a regular expression my_reg that captures # the Kingdom, 
# Phylum and Species name for each species and prints it out neatly:

# my_reg = ??????

# Hint: you will probably want to use re.findall(my_reg, text)...
# Keep in mind that there are multiple ways to skin this cat! 

############ trying to use loops 
#kingdom = []
#for x in text:
	#if x == "Kingdom":
		#kingdom[re.search("\w*\s")]
		#print kingdom

############ using lookbehind in regex
# Use lookbehind, match sth that is preceded by sth, in this case preceded by 
# Kingdom, Phylum or Species. The sintax is ?<= the you put the thing that you want 
# to be the "precedent" and then the expresion to match


Kingdom = re.findall(r"(?<=Kingdom\s)\w+", text)
Phylum = re.findall(r"(?<=Phylum\s)\w+", text)
Species = re.findall(r"(?<=Species\s)\w+\s\w+", text)

# Makes a neat list, with spaces in between 

for i in range(len(Kingdom)):
	print Kingdom[i] + " " + Phylum[i] + " " + Species[i]


List = [[Kingdom],[Phylum],[Species]]
