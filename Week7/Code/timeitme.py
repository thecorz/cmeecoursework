

""" 
Compare the speed of different fuctions and expressions in python.
To run it you should import all the functions in the module (this file)
using: from timeitme.py import *
this imports all the functions so you don't have to copy and paste all 
the functions manually
"""
# range vs. xrange

import time

def a_not_useful_function():
	"""Test speed in function range"""
	y = 0
	for i in range(100000):
		y = y + i
	return 0

def a_less_useless_function():
	"""Test speed of function xrange"""
	y = 0
	for i in xrange(100000):
		y = y + i
	return 0

# One approach is to time it like this

start = time.time()
a_not_useful_function()
print "a_not_useful_function takes %f s to run." % (time.time() - start)

start = time.time()
a_less_useless_function()
print "a_less_useless_function takes %f s to run." % (time.time() - start)

#################### for loops vs. list comprehensions

my_list = range(1000)

def my_squares_loop(x):
	"""Test speed of loops"""
	out = []
	for i in x:
		out.append(i ** 2)
	return out

def my_squares_lc(x):
	"""Test speed of list comprenhensions"""
	out = [i ** 2 for i in x]
	return out
	
##################### for loops vs. join method

import string
my_letters = list(string.ascii_lowercase) # creates a list with all the ascii letters

def my_join_loop(l):
	""" Adds letters to a empty object called out"""
	out = '' # creates an object called "out"
	for letter in l: # l is the list "my_letters" it will be the imput of the function
		out += letter # adds every letter to the object "out"
	return out

def my_join_method(l):
	""" Adds letters using the function join"""
	out = ''. join(l) # adds the letters in the list to "out"
	return out

#################### Oh dear

def getting_silly_pi():
	"""Test of timeit"""
	y = 0
	for i in xrange(100000):
		y = y + i
	return 0
	
def getting_silly_pi():
	"""Test of timeit"""
	y = 0
	for i in xrange(100000):
		y += i
	return 0



