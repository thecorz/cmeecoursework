#!/usr/bin/python
import cProfile
import os
import subprocess
"""Opens LV1.py and LV2.py and profile them"""

############Several ways of running a filing withing a script

### execfile
#execfile("LV2.py 1. 0.1 1.5 0.75 100")
#execfile("LV1.py")

#### os.system
# os.system("python LV1.py")

### subprocess
#subprocess.call("LV1.py")


############## PROFILING

####### Using cProfile
#You may wonder what is that "&" doing at the end of each line. Without it the line command wouldn't work if there is a graph open. So you would have to close the graph in order to continue doing something on the command line. Using "&" that doesn't happen


### Pipe the output of the profile in a file
#subprocess.Popen("python -m cProfile -s tottime LV1.py > ../Results/cProfile_LV1.txt &", shell = True).wait()
#subprocess.Popen("python -m cProfile -s tottime LV2.py 1. 0.1 1.5 0.75 100 > ../Results/cProfile_LV2.txt &", shell = True).wait()

### Print the results on screen

# "-s tottime" order the report by total time consummed"
subprocess.os.system("python -m cProfile -s tottime LV1.py &")
subprocess.os.system("python -m cProfile -s tottime LV2.py 1. 0.1 1.5 0.75 100 &")
subprocess.os.system("python -m cProfile -s tottime LV3.py &")
subprocess.os.system("python -m cProfile -s tottime LV4.py &")
############### RESULTS

# If you 
