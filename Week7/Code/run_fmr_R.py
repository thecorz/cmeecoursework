#!/usr/bin/python
"""Runs fmr.R and prints whether the run was successful and the R output"""

import subprocess
import os.path
subprocess.Popen("Rscript --verbose fmr.R", shell=True).wait()

if os.path.isfile("../Results/fmr_plot.pdf") == True:
	print "Plotting succesful"

else:
	print "Error in plotting"

if os.path.isfile("../Results/species.csv") == True:
	print "species.csv created successfully"

else:
	print "Error creating species.csv"
	
