"""Shows the use of Rscript in Python"""
import subprocess

# There is two ways of doing this. The terminal have to look for command
# Rscript somewhere. Usually it is in /usr/lib/R/bin/Rscript. But usually
# Rscript is in the bin directory. Remember that the bin directory is where
# the commands for the terminal are stored. Thats why in this case you can
# call Rscript straight from the terminal without typing down the whole path.
# Why does'nt it work just typing R as we do when we run R in Bash? because
# just "R" was considered too short

subprocess.Popen("Rscript --verbose TestR.R > \
../Results/TestR.Rout 2> ../Results/TestR_errFile.Rout",\
shell=True).wait()

# Look at the sintax, it uses the ">" and "2>" simbol which directs the output
# to a file. These are the bash pipes to put things in files or add things
# to files.  
