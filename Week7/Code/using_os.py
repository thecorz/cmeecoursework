""" Creates a txt file with a list of files and subdirectories that start with the letter upper case C
Creates a txt file with a list of files and subdirectories that start with either upper or lower case C
Creates a txt file with a list of subdirectories that start with either upper or lower case C
"""

# Use the subprocess.os module to get a list of files and directories 
# Hint: look in subprocess.os and/or subprocess.os.path and/or 
# subprocess.os.walk for helpful functions

import subprocess

#################################
#~Get a list of files and 
#~directories in your home/ that start with an uppercase 'C'

# Type your code here:
 
# Get the user's home directory.
home = subprocess.os.path.expanduser("~")

# Create a list to store the results.
FilesDirsStartingWithC = []

# Use a for loop to walk through the home directory.
for (dirs, subdirs, files) in subprocess.os.walk(home):
	for subdir in subdirs:
		if subdir.startswith('C'):
			FilesDirsStartingWithC.append(subdir)
	for file in files:
		if file.startswith('C'):
			FilesDirsStartingWithC.append(file)

a = str(FilesDirsStartingWithC)
f = open("../Results/FilesDirsStartingWithC.txt", 'w')
f.write(a) 
	
#################################
# Get files and directories in your home/ that start with either an 
# upper or lower case 'C'

# Type your code here:
home = subprocess.os.path.expanduser("~")
FilesDirsStartingWithCandc = []

for (dirs, subdirs, files) in subprocess.os.walk(home):
	for subdir in subdirs:
		if subdir.lower().startswith('c'):
			FilesDirsStartingWithCandc.append(subdir)
	for file in files:
		if file.lower().startswith('c'):
			FilesDirsStartingWithCandc.append(file)
			
a = str(FilesDirsStartingWithCandc)
f = open("../Results/FilesDirsStartingWithCandc.txt", 'w')
f.write(a) 

#################################
# Get only directories in your home/ that start with either an upper or 
#~lower case 'C' 

# Type your code here:

home = subprocess.os.path.expanduser("~")
DirsStartingWithCandc = []

for (dirs, subdirs, files) in subprocess.os.walk(home):
	for subdir in subdirs:
		if subdir.lower().startswith('c'):
			DirsStartingWithCandc.append(subdir)
			
a = str(DirsStartingWithCandc)
f = open("../Results/DirsStartingWithCandc.txt", 'w')
f.write(a) 
