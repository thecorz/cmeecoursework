import networkx as nx
import scipy as sc
import matplotlib.pyplot as plt
import csv
import pandas as pd
import numpy as np
f = open('../Data/QMEE_Net_Mat_edges.csv', 'r')

#import edges as an array. I loose the names of the members because they are strings.
# If you use dtype None you don't loose any data because it doesn't transform the data to any kind of data
edges = sc.genfromtxt(f, delimiter=',', dtype = None)

# I was trying to get the indices but it didn't work
#a = np.where(edges != 0)

# I put the names of the institutions in a separated list
nodes_edges = edges[0,:]

# nx doesn't work if the nodes are in an array. tolist transforms it in a list
nodes_edges = nodes_edges.tolist()

# I need to delete the name of the institutions in order to get a simetrical matrix. Otherwise when I loop through later the indices get out of range
edges2 = np.delete(edges, 0, 0)

# I had the problem that the numbers are not treated as numbers, so the if function didn't work. I tried to converse the data to numbers using .astype, and it says that the data are now int but the array still says that is numpy.string

# God dammit! It didn't work because I was using the command .astype on my array edges2 but I didn't specify to save it in any object so it wasn't actually changing edges2 at all. That's why the following line of code doesn't work but the next one does. fucking hell!
#edges2.astype('u4', casting= 'unsafe')

edges2 = edges2.astype(np.int)
# np.fromstring doesn't seem to work
#np.fromstring(edges2)

# create the list where to put the interactions
tuples = []
edges_strength = []
# Here what i do is instead of iterape over the actual array edges2 I am goint to use a fictional array of the same size of edges2 just to take the indices. I will use this indices to index the edges2 array. 
for i in range(len(edges2)):
	for j in range(len(edges2[0])):
		# if that element is not 0 then take the institution's names from the institution list and make a tuple with it and add to the list of tuples
		if edges2[i,j] > 0:
			tuples = tuples + [(nodes_edges[i],nodes_edges[j])]
			# edges weight 
			edges_strength = edges_strength + [edges2[i,j]/2]

tuples

##Another way of importing, but then I can not access the content of each column for some stupid reason
#edges = sc.recfromcsv(f, delimiter=',')
#edges = sc.array(edges)

g = open('../Data/QMEE_Net_Mat_nodes.csv', 'r')
#nodes = sc.recfromcsv(g, delimiter=',')
#nodes = sc.genfromtxt(g, delimiter=',')
#nodes = sc.array(nodes)

#Import the csv as a data.frame using pandas.
#If I want to work with data of different kind I need a data frame because otherwise if I import the csv file as an array, it loose some of the data because the data has to be of the same kind.
nodes = pd.read_csv('../Data/QMEE_Net_Mat_nodes.csv') #Import the csv as a data.frame

# Takes the names of the nodes from nodes. I need iloc to do that because it doesn't work like an array
Sps = nodes.iloc[:,0]

# PI number of Principal Investigators in each institution. I created a list with them to make the size of the nodes according to the number of PI
PIs = nodes.iloc[:,2]*100

# list of colors for the nodes. the colors relate to the kind of institution the node belongs to. They are in the same order as the appear in the node list
Colors = ['b','b','g','g','g','r']

# I tried using loops to asign colors to the different kinds of institution
#for i in Sps:
	#if i == "University":
		#Colors.append['b']
	#if i == "Hosting Partner":
		#Colors.append['g']
	#if i == "Non-Hosting Partners":
		#Colors.append['r']
		
# close all the graphics
plt.close('all')

# Creates an empty graphic
G = nx.Graph()

# Asign a particular kind of graphic, pos is a parameter that nx needs in odert to plot the graph
pos = nx.circular_layout(nodes_edges)

# You could add different parts of the graph gradually, but I am doing it here all at once with nx.draw.network, that has all the options I need inside the function.
# nodelist = nodes
nx.draw_networkx(G, pos, nodelist = nodes_edges, node_size = PIs, node_color = Colors, with_labels = True, arrows = True, edgelist = tuples, width = edges_strength)

## Another way to add nodes and edges to the graph
#G.add_nodes_from(nodes_edges)
#G.add_edges_from(tuples)


nx.draw(G)
#plt.show()
plt.savefig("../Results/QMEE_CDT.svg")

