#!/usr/bin/python

""" The discrete-time version of the Lotka-Volterra Model simulated using scipy """

""" Returns the growth rate of predator and prey populations at any 
given  """

import scipy as sc # short the name
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting
import sys
import numpy as np

#Parameters
r = 1. # Resource growth rate
a = 0.1 # Consumer search rate (determines consumption rate)
z = 1.5 # Consumer mortality rate
e = 0.75 # Consumer production efficienc
k = 100. # Carrying capacity

#Get the randomness fixed
np.random.seed(1234)


# Time steps
t = 100

# Initial populations
x0 = 10 #Resource
y0 = 5 	#Consumer

# Creates an array where x0 and y0 are columns. The populations sizes R and C
# are agrupated in two like (R, C) per time step. In ordert get that we have
# to use double brackets, otherwise, if we use single brackets we get just a list
# of data without columns
t0 = 0
pops = sc.array([[x0, y0, t0]])

#pops = numpy.matrix([[x0, y0, t0]])
#pops.reshape((-1,2)) # initials conditions: 10 Resource and 5 Consumer per unit area

# Because we are using double brackets, 2D array we need two indices
R = pops[0][0]
C = pops[0][1]

# it's a discrete function. You calculate the next pop size from the previous pop size
# The are labeled the same in this loop, because in every iteartion of the loop
# C and R values for every time step get stored in the array "pops", then they are used
# to calculate the next pop size. 
for i in range(1, t):
	#introduces random fluctuation
	s0 = np.random.normal(0, 0.05, 1) # if s > 0.05 the function generates R pops too big
	s = s0[0]
	R = (pops[i-1,0])*(1+((r+s)*(1-((pops[i-1,0])/k))) - a*(pops[i-1, 1])) 
	C = (pops[i-1, 1])*(1-z+s+e*a*(pops[i-1, 0]))
	#R = R*(1+((r+s)*(1-(R/k))) - a*C) 
	#C = C*(1-z+e*a*R)
	if R < 0: 
		break
	pop = [[R, C, i]]
	pops = sc.append(pops, pop, axis=0) # axis=0 so it append it as rows



print "The final populations of Consumer and Rerource are"
print pops[-1,:]

prey, predators, time = pops.T # What's this for? Transpone
f1 = p.figure() #Open empty figure object called p
p.plot(time, prey, 'g-', label='Resource population') # Plot
p.plot(time, predators  , 'b-', label='Consumer population')
p.grid()

p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics') # (r), (a), (z), (e))
p.text(30, 1, 'r = {0}    a = {1}    z = {2}    e = {3}    k = {4}'.format(r, a, z, e, k))
p.show()
f1.savefig('../Results/prey_and_predators_3.pdf') #Save figure
