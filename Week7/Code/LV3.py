#!/usr/bin/python

""" The typical Lotka-Volterra continuous time Model simulated using scipy,

showing the final population sizes """

import scipy as sc # short the name
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting
import sys

# import matplotlip.pylab as p #Some people might need to do this

def dR_dt(pops, t=0):
    """ Returns the growth rate of predator and prey populations at any 
    given time step """
    # We add an extra parameter k the carring capacity of the environment
    # it's dividing R so when R = pop is getting similar to K the fraction
    # gets similar to 1 and after the substraction with 1 we get a very small
    # number multipliying the growth of the population (rR)
    
    R = pops[0]
    C = pops[1]
    dRdt = r*R*(1-(R/k)) - a*R*C 
    dCdt = -z*C + e*a*R*C
    
    return sc.array([dRdt, dCdt])

r = float(sys.argv[1]) # Resource growth rate
a = float(sys.argv[2]) # Consumer search rate (determines consumption rate) 
z = float(sys.argv[3]) # Consumer mortality rate
e = float(sys.argv[4]) # Consumer production efficiency
k = float(sys.argv[5]) # Carrying capacity of the ecosystem (50)


# Define parameters:
#r = 1. # Resource growth rate
#a = 0.1 # Consumer search rate (determines consumption rate) 
#z = 1.5 # Consumer mortality rate
#e = 0.75 # Consumer production efficiency


# Now define time -- integrate from 0 to 15, using 1000 points:
# 15 seconds, days whatever units then subdivide it 1000 times
t = sc.linspace(0, 100,  1000)

x0 = 10
y0 = 5 
z0 = sc.array([x0, y0]) # initials conditions: 10 prey and 5 predators per unit area

# integrate a ordinary diferential ecuations (odeint)
pops, infodict = integrate.odeint(dR_dt, z0, t, full_output=True)
print "The final populations of Consumer and Rerource are"
print pops[-1,:]
# python is gonna return a dictionary
infodict['message']     # >>> 'Integration successful.'

prey, predators = pops.T # What's this for? Transpone
f1 = p.figure() #Open empty figure object called p
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.grid()

p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics') # (r), (a), (z), (e))
p.text(30, 1, 'r = {0}    a = {1}    z = {2}    e = {3}    k = {4}'.format(r, a, z, e, k))
p.show()
f1.savefig('../Results/prey_and_predators_2.pdf') #Save figure
