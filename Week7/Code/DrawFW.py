"""Generates a circular network with different body sizes"""

import networkx as nx
import scipy as sc
import matplotlib.pyplot as plt

def GenRdmAdjList(N= 2, C = 0.5):
	"""
	Generate random adjancency list fiven N nodes with connectance
	probablility C
	"""
	Ids = range(N) # N Range of numbers = N number of sps in the network
	ALst = [] # List of pair of numbers = pair of sps = interacction 
	for i in Ids:
		if sc.random.uniform(0,1,1) < C: # take a random number from the normal distribution. If the number is smaller than the conectance
			Lnk = sc.random.choice(Ids,2).tolist() # take two nambers at random from the list Ids and transform them in a list in Lnk, the list has just one item: the pair of numbers = the link between the sps. otherwise they are an array
			if Lnk[0] !=Lnk[1]: #avoid self loops
				ALst.append(Lnk) # include Lnk=the pair of numbers in the list
	return ALst
	
## Assign body mass range
SizRan = ([-10,10]) # use log scale

## Assign number of species (MaxN) and conectance (C). You can vary this to get more species and more edges
MaxN = 30
C = 0.75

## Generate adjancency list unsing the function:
AdjL= sc.array(GenRdmAdjList(MaxN, C))

## Generate species (node) data:
Sps = sc.unique(AdjL) # get species ids from the adjancency list
Sizs = sc.random.uniform(SizRan[0], SizRan[1],MaxN) # Generate body sizes (log10 scale)

######## The Plotting ########
plt.close('all')

##Plot using networkx:

## Calculate coordinates for circular configuration:
## (see networkx.layout for inbuilt function to pumpute other types of node coords)

pos = nx.circular_layout(Sps)

G = nx.Graph()
G.add_nodes_from(Sps)
G.add_edges_from(tuple(AdjL))
NodSizs= 100**-32 + (Sizs-min(Sizs))/(max(Sizs)-min(Sizs)) # node sizes in proportion to body sizes
nx.draw(G, pos, node_size = NodSizs*1000)
plt.show()
