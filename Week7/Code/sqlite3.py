"""Shows the usage of sqlite3"""

# import the sqlite3 library
import sqlite3

# create a connection to the database
conn = sqlite3.connect('../Data/test.db')

# to execute commands, create a "cursor"
c = conn.cursor()

# use the cursor to execute the queries
# use the triple single quote to write
# queries on several lines

c.execute('''Create table Test
			(ID INTEGER PRIMARY KEY,
			MyVal1 INTEGER,
			MyVal2 TEXT)''')
			
c.execute('''DROP TABLE Test''')
			
# insert the records, note that because we set the primary key in the previous command
#, it will auto-increment automatically, so we 

			
			
c.execute('''INSERT INTO Test VALUES (NULL, 3, 'mickey')''')

c.execute('''INSERT INTO Test VALUES (NULL, 4, 'mouse')''')

conn.commit()

c.execute("SELECT * FROM TEST")

print c.fetchone()
