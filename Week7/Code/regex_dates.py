#!/usr/bin/python
"""Finds a date in a string"""


# Write a regex to match dates in format YYYYMMDD, making sure that:
	Only seemingly valid dates match (i.e., year greater than 1900)
	First digit in month is either 0 or 1
	First digit in day ≤ 3







import re


dates = "1802/10/21  1902/21/62 1902/05/55 2001/12/02 1902/05/21"


# dates without slashes
a_dates = "19990203 20010506"
# matches dates without slashes
match = re.search(r"(?(1)9)(?(2)0)\d{1,3}[10]\d[0123]\d", a_dates)

match.group()
# I tryied to used conditionals but I couldn't work it out so I wil use lookahead
# Trying to use lookahead in regex for years
match = re.search(r"(^(1(?=9))|^(2(?=0)))\d{3}\/[10]\d\/[0123]\d", dates)

# Using ( | ) expression: match one or the other. You can use it more than one time like it is done here for the days.
# It works but it only finds the first match

match = re.search(r"(19|20)\d\d/(0[1-9]|1[0-2])/(0[1-9]|1[0-9]|2[0-9]|3[0-1])", dates)

match.group()

match = re.findall(r"(19|20)\d\d/(0[1-9]|1[0-2])/(0[1-9]|1[0-9]|2[0-9]|3[0-1])", dates)
# 
print match

# using look ahead

match = re.search(r"(^(?=9)|^(2(?=0)))[0-9]{3}((1(?=0))|(1(?=1))|(1(?=2))|0[0-9]{1}((3(?=0))|(3(?=1))|[0-2])[0-9]{1}
