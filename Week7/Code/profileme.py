"""Example of usage of profiling"""


def a_useless_function(x):
	"""A useless function"""	
	y = 0
	# eight zeros
	for i in xrange(100000000):
		y = y + i
	return 0

def a_less_useless_function(x):
	"""a useless functions"""
	y = 0
	for i in xrange(100000):
		y = y + i
	return 0
	
def some_function(x):
	"""show the time of each function"""
	print x
	a_useless_function(x)
	a_less_useless_function(x)
	return 0

some_function(1000)
