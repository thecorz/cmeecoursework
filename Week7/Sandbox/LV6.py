#!/usr/bin/python

""" The discrete-time version of the Lotka-Volterra Model simulated using scipy """

""" Returns the growth rate of predator and prey populations at any 
given  """

import scipy as sc # short the name

#Parameters
r = 0. # Resource growth rate
a = 0. # Consumer search rate (determines consumption rate)
z = 0. # Consumer mortality rate
e = 0. # Consumer production efficienc
k = 0. # Carrying capacity

# Time steps
t = 30

# Initial populations
x0 = 500. #Resource
y0 = 200. 	#Consumer

# Creates an array where x0 and y0 are columns. The populations sizes R and C
# are agrupated in two like (R, C) per time step. In ordert get that we have
# to use double brackets, otherwise, if we use single brackets we get just a list
# of data without columns
#t0 = 0
#pops = sc.array([[x0, y0, t0]])

#pops = numpy.matrix([[x0, y0, t0]])
#pops.reshape((-1,2)) # initials conditions: 10 Resource and 5 Consumer per unit area

# Because we are using double brackets, 2D array we need two indices
#R = pops[0][0]
#C = pops[0][1]

# it's a discrete function. You calculate the next pop size from the previous pop size
# The are labeled the same in this loop, because in every iteartion of the loop
# C and R values for every time step get stored in the array "pops", then they are used
# to calculate the next pop size. 
Parameters = sc.array([[r, a, z, e, k]])
for r in sc.linspace(0.1, 5., 50):
	for a in sc.linspace(0.1, 2., 20):
		for z in sc.linspace(0.5, 5., 20):
			for e in sc.linspace(0.5, 10., 20):
				for k in sc.linspace(500, 4500., 21.):
					pops = sc.array([[x0, y0]])
					for i in range(1, t):
						#import ipdb; ipdb.set_trace()
						#pops2 = sc.array([[x0, y0, t0]])						
						R = (pops[i-1,0])*(1+r*(1-((pops[i-1,0])/k)) - a*(pops[i-1, 1])) 
						C = (pops[i-1, 1])*(1-z+e*a*(pops[i-1, 0]))
						pop = [[R, C]]
						pops = sc.append(pops, pop, axis=0) # axis=0 so it append it as rows
						if i > 20:
							if R < 1000000:
								Parameter = [[r, a, z, e, k]]
								Parameters = sc.append(Parameters, Parameter, axis=0) 
								print r, a, z, e, k
#252000000 of iterations

#print "The final populations of Consumer and Rerource are"
#print pops[-1,:]

#prey, predators, time = pops.T # What's this for? Transpone
#f1 = p.figure() #Open empty figure object called p
#p.plot(time, prey, 'g-', label='Resource population') # Plot
#p.plot(time, predators  , 'b-', label='Consumer population')
#p.grid()

#p.legend(loc='best')
#p.xlabel('Time')
#p.ylabel('Population')
#p.title('Consumer-Resource population dynamics') # (r), (a), (z), (e))
#p.text(30, 1, 'r = {0}    a = {1}    z = {2}    e = {3}    k = {4}'.format(r, a, z, e, k))
#p.show()
#f1.savefig('../Results/prey_and_predators_3.pdf') #Save figure
