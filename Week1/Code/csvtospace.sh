#!/bin/bash
#Author: Javier Cuadrado Corz j.cuadrado-corz16@imperial.ac.uk
#Script: cvstospaces.sh
#Desc: Substitues the commas inside of a file for tabs
#Date: Oct 2016

echo "Creating a space delimited version of $1"
cat $1 | tr -s "," "\t" >> $1.tsv

echo "Finished"

exit
