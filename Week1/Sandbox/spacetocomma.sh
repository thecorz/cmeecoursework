#!/bin/bash
# Author: Javier j.cuadrado-corz16@imperial.ac.uk
# Script: tabtocsv.sh
# Desc: substitute the spaces in the files with commas
#       saves the output into a .csv file
# Arguments: 1-> tab delimited file
# Date: Oct 2016

echo "Creating a comma delimited version of $1 ..."

cat $1 | tr -s " " "," >> $1.csv1

echo "Done!"

exit
