## using "tapply"

x <- 1:20
y <- factor(rep(letters[1:5], each = 4))

tapply(x, y, sum)


## using "by"
# import some data
attach(iris)
print(iris)

# use colMeans (as it is better for dataframe)
by(iris[,1:2], iris$Species, colMeans)
by(iris[,1:2], iris$Petal.Width, colMeans)

## Using "replicate"

print(replicate(10, runif(5)))
