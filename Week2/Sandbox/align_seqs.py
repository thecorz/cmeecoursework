# These are the two sequences to match
import csv

f = open('../Data/sequences.csv','rb') # Opens the file
sequences = csv.reader(f)	# Reads the csv file
sequences_list = []		# Makes a list

for column in sequences:	# Adds the sequences to the list
		sequences_list.append(column)

# seq1 = column[0]
# seq2 = column[1] 

seq1 = sequences_list[0][0]
seq2 = sequences_list[0][1]


#seq1 = "CAATTCGGAT"
#seq2 = "ATCGCCGGATTACGGG"

# assign the longest sequence s1, and the shortest to s2
# l1 is the length of the longest, l2 that of the shortest

l1 = len(seq1)
l2 = len(seq2)
if l1 >= l2:
    s1 = seq1
    s2 = seq2
else:   # s1 is always gonna be the longest and s2 the shortest
    s1 = seq2
    s2 = seq1
    l1, l2 = l2, l1 # swap the two lengths to make l1 always the longest and l2 always the shortest

# function that computes a score
# by returning the number of matches 
# starting from arbitrary startpoint
def calculate_score(s1, s2, l1, l2, startpoint):
    # startpoint is the point at which we want to start
    matched = "" # contains string for alignement
    score = 0
    for i in range(l2): #for each base in s2
        if (i + startpoint) < l1: 
			# if the position of the matching base in s2 is out of the
			# range of s1 it does not do the matching. no just that,
			# Because it can't do the matching because there is not base
			# in L1 to match without this if it wouldn't do that cycle
			# or particular loop. 
			
			
            # does the matching of the character
            if s1[i + startpoint] == s2[i]:
                matched = matched + "*" # writes a * for every match
                score = score + 1	# ads 1 to the score
            else:
                matched = matched + "-" # writes a - when they dont match

    # build some formatted output
    print "start point =", startpoint
    print "." * startpoint + matched           
    print "." * startpoint + s2
    print s1
    print "score =", score 
    print ""

    return score

#calculate_score(s1, s2, l1, l2, 0)
#calculate_score(s1, s2, l1, l2, 1)
#calculate_score(s1, s2, l1, l2, 5)

## now try to find the best match (highest score)
my_best_align = None
my_best_score = -1

for i in range(l1):
    z = calculate_score(s1, s2, l1, l2, i)
    if z > my_best_score:
        my_best_align = "." * i + s2
        my_best_score = z

print "The best align" + "\n" + my_best_align
print s1
print "Best score:", my_best_score

f = open('../Data/bestsequence.txt','w')
f.write('The best align' + '\n' + my_best_align + '\n' + s1 + '\n\n' + 'The best score is ' + str(my_best_score))
# argv for files said Samraat
