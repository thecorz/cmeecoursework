#!/usr/bin/python

""" From the set given 'birds' make a different list for
the scientific name, the common name and the weight of the birds.
Author Javier Cuadrado Corz
"""

	





birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )

#(1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses for
# each species in birds, respectively. 

# (2) Now do the same using conventional loops (you can shoose do this 
# before 1 !). 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

# (2)
weight = []
common_name = []
scientific_name = [] # Define a list
for species in birds:	# Creates a loop looking a every tuple in birds
	scientific_name.append(species[0]) # Adds a the first item of the tuple to this list
	common_name.append(species[1]) # Adds the second item of the tuple to this list
	weight.append(species[2]) # Adds the third item of the tuple to this list
print scientific_name
print common_name
print weight

## (1)

weight = [species[2] for species in birds]
print weight

common_name = [species[1] for species in birds]
print common_name

scientific_name = [species[0] for species in birds]
print scientific_name

