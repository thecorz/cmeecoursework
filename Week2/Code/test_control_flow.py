#!/usr/bin/python

"""Some functions exemplifying the use of control statements"""
#docstrings are considered part of the running code (normal comments are
#stripped). Hence, you can access your docstrings at run time.
__author__ = 'Samraat Pawar (s.pawar@imperial.ac.uk)'
__version__ = '0.0.1'

import sys
import doctest #import the doctest module

	
def even_or_odd(x=0):	
	"""Find whether a number x is even or odd.
	>>> even_or_odd(10)
	'10 is Even!'
	
	>>> even_or_odd(5)
	'5 is Odd!'
	
	whenever a float is provided, then the closest integer is used:
	>>> even_or_odd(3.2)
	'3 is Odd!'
	
	in case of negative numbers, the positive is taken:
	>>> even_or_odd(-2)
	'-2 is Even!'
	
	"""
	#Define function to be to tested
	if x % 2 == 0:
		return "%d is Even!" % x #%d is like $1 in unix, refers to a variable that has been given before
	return "%d is Odd!" % x

## I SUPPRESSED THIS BLOCK: WHY?


#if (__name__== "__main__"):
	#status = main(sys.argv)
	#sys.exit(status)

doctest.testmod()   # To run with embedded test
# aternativily you can try python -m doctest -v test_control_flow.py in the unix terminal

	
			
