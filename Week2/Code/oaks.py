"""
Shows the use of list comprenhensions
"""

## [] for making a list

taxa = [ 'quercus robut',
		'Fraxinus excelsior',
		'Pinus Sylvestris',
		'Quercus cerris',
		'Quercus petraea',
		]
## 1. Define a function with the name is_an_oak that needs data (name)
## 2. return takes whatever data to use it later. here is taking name.
## 3. .lower is converting it in lower case.
## 4. startwith is a function like if. if this string starts with... 

def is_an_oak(name):
	"""Identify oaks in a object"""
	return name.lower().startswith('quercus ')

	
oaks_loops = set() #Set a set
for species in taxa: #Set a loop define the items to pass through the loop 
	if is_an_oak(species): 
		oaks_loops.add(species) #adds the to the set 
print oaks_loops

##Using list comprehensions | It does the same thing that the previous module
## but it's more compact. []for making a list and then you transform it
## in a set. The first species is indicating to add that information to 
## the list. 


oaks_lc = set([species for species in taxa if is_an_oak(species)])
print oaks_lc

##Get names in UPPER CASE using for loops
oaks_loops = set()
for species in taxa:
	if is_an_oak(species):
		oaks_loops.add(species.upper())
print oaks_loops

##Get names in UPPER CASE using list comprehensions
oaks_lc = set([species.upper() for species in taxa if is_an_oak(species)])
print oaks_lc
	
