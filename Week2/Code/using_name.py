#!/usr/bin/python
# Filename:using_name.py
""" Show the use of some lines of code for when you want to use a script with functions that can be use in other scripts """


if __name__== '__main__':
	print 'this program is being run by itself'
else:
	print 'I am being imported from another module'
