#!/usr/bin/python
"""
From a list of tuples with species and its order,
the loop creates a dictionary

"""

taxa = [ ('Myotis lucifugus','Chiroptera'),
         ('Gerbillus henleyi','Rodentia',),
         ('Peromyscus crinitus', 'Rodentia'),
         ('Mus domesticus', 'Rodentia'),
         ('Cleithrionomys rutilus', 'Rodentia'),
         ('Microgale dobsoni', 'Afrosoricida'),
         ('Microgale talazaci', 'Afrosoricida'),
         ('Lyacon pictus', 'Carnivora'),
         ('Arctocephalus gazella', 'Carnivora'),
         ('Canis lupus', 'Carnivora'),
        ]

# Write a short python script to populate a dictionary called taxa_dic 
# derived from  taxa so that it maps order names to sets of taxa. 
# E.g. 'Chiroptera' : set(['Myotis lucifugus']) etc. 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

# Write your script here:

taxa_dic = {} # Creates a dictionary

orders = set([i[1] for i in taxa]) 
# It's a list comprenhension that creates a list of orders. But because
#there are several species in each order, we need a set of just unique orders.
#we dont want repeated orders in the list
					

for eachorder in orders: #loop that goes through each order in the list
	taxa_dic[eachorder] = set([speciesorder[0] for speciesorder in taxa if speciesorder[1] is eachorder]) 
# eachorder is the key of the dictionary
# list comprehension that adds a species if the species of the tuple in taxa is equals to the order 
#in the loop

print taxa_dic
print taxa_dic["Rodentia"]
