#!/usr/bin/python

"""
There is a tuple list of months and avarage UK rainfall(mm)
The first two loops are list comprenhension that create 
1. a list of the months and rainfall that are over 100 mm
2. a list of the months whose rainfall is less than 50mm

The second two loops are the same but using conventional loops
1. list of tuples over 100mm
2. list of months less thatn 50mm

"""
 

# Average UK Rainfall (mm) for 1910 by month
# http://www.metoffice.gov.uk/climate/uk/datasets
rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),
           )

# (1) Use a list comprehension to create a list of month,rainfall tuples where
# the amount of rain was greater than 100 mm.


over_100 = [month for month in rainfall if month[1] > 100]
print over_100
# (2) Use a list comprehension to create a list of just month names where the
# amount of rain was less than 50 mm. 

less_50 = [month[0] for month in rainfall if month[1] < 50]
print less_50

# (3) Now do (1) and (2) using conventional loops (you can choose to do 
# this before 1 and 2 !). 

# (1) using conventional loops
over_100 = [] #Creates a list
for each_month in rainfall: #Loop
	if each_month[1] > 100: # if in each tuple the rainfalll is greater than 100
		over_100.append(each_month) #add the tuple to the tuple list

print over_100

# (2) using conventional loops

less_50 = [] #Creates the list
for each_month in rainfall: #Loop
	if each_month[1] < 50: #If in each tuple the rainfall is less than 50
		less_50.append(each_month[0]) #add the month of the tuple to the list
print less_50

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS
