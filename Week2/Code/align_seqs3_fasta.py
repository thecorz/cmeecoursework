# These are the two sequences to match
""" Align seq from external files using one big function and puting all the 
functions within that big one
"""

import re

def extract_seq(file1, file2):
	f = open(file1,'r')
	line = f.readline()
	meta1 = ""
	seq1 = ""
	while line:
		line = line.rstrip('\n')
		if '>' in line:
			meta1 = line
		else:
			seq1 = sequence + line
	line = f.readline()
	f.close
	
	g = open(file2,'r')
	line = g.readline()
	meta2 = ""
	seq2 = ""
	while line:
		line = line.rstrip('\n')
		if '>' in line:
			meta2 = line
		else:
			seq2 = sequence + line
	line = g.readline()
	g.close
	
# assign the longest sequence s1, and the shortest to s2
# l1 is the length of the longest, l2 that of the shortest
	
	
	l1 = len(seq1)
	l2 = len(seq2)
	if l1 >= l2:
		s1 = seq1
		s2 = seq2
	else:   # s1 is always gonna be the longest and s2 the shortest
		s1 = seq2
		s2 = seq1
		l1, l2 = l2, l1 # swap the two lengths to make l1 always the longest and l2 always the shortest
	
	# function that computes a score
	# by returning the number of matches 
	# starting from arbitrary startpoint
	def calculate_score(s1, s2, l1, l2, startpoint):
	    # startpoint is the point in l1 at which we want to start comparing the bases with l2
	    matched = "" # contains a string of symbols that visualises the alignement
	    score = 0	#creates a score which is where we are gonna add the number of bases matched
	    for i in range(l2): #for each base in s2
	        if (i + startpoint) < l1: 
				# if the position of the matching base in s2 is out of the
				# range of s1 it does not do the matching no symbols needed. no just that,
				# Because it can't do the matching because there is not base
				# in L1 to match, without this bit of code it wouldn't do that cycle
				# of the loop. 
				
				
	            # does the matching of the character
	            if s1[i + startpoint] == s2[i]:
	                matched = matched + "*" # writes a * for every match
	                score = score + 1	# ads 1 to the score
	            else:
	                matched = matched + "-" # writes a - when they dont match
	
	    # build some formatted output
	    print "start point =", startpoint
	    print "." * startpoint + matched           
	    print "." * startpoint + s2
	    print s1
	    print "score =", score 
	    print ""
	
	    return score
	
	#calculate_score(s1, s2, l1, l2, 0)
	#calculate_score(s1, s2, l1, l2, 1)
	#calculate_score(s1, s2, l1, l2, 5)
	
	## now try to find the best match (highest score)
	my_best_align = None
	my_best_score = -1
	
	for i in range(l1):
		z = calculate_score(s1, s2, l1, l2, i)
		if z > my_best_score:
			my_best_align = "." * i + s2
			my_best_score = z
	
	print "\nThe best align\n\n" + my_best_align
	print s1
	print "\nBest score:", my_best_score

	f = open('../Data/bestsequence.txt','w')
	f.write('The best align' + '\n' + my_best_align + '\n' + s1 + '\n\n' + 'The best score is ' + str(my_best_score))
	f.close
	
	

if (__name__== "__main__"):
	import sys
	extract_seq(sys.argv[1],sys.argv[2])
	

	

