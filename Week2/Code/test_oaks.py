import csv
import sys
import pdb
import doctest

"""There was a bug in this module. Using pdb and doctest it's been found and corrected
"""

#Define function
def is_an_oak(name):
	
	# This is for doctest to see if all the bugs have been solved
    """ Returns True if name is starts with 'quercus '
        >>> is_an_oak('quercus')
        True
        >>> is_an_oak('Fraxinus excelsior')
        False
        >>> is_an_oak('Fagus sylvatica')
        False
        >>> is_an_oak('Quercuss')
        False
    """
    return name.lower() =='quercus' 

#There was a space after quercus. It's been deleted
#The original file used a function startswith to identify the quercus.
#But that function didn't work because in order to work it need a space
#after quercus and the imput file, testoaksdata, there is not space after quercues because 
#quercus and the specific name are separated and the sp
    
  
    
print(is_an_oak.__doc__)

def main(argv): 
    f = open('../Data/TestOaksData.csv','rb') # the relative directory was wrong
    g = open('../Data/JustOaksData.csv','wb') # the relative directory was wrong
    taxa = csv.reader(f)
    csvwrite = csv.writer(g)
    oaks = set()
    for row in taxa:
        print row
        print "The genus is", row[0]
        if is_an_oak(row[0]): 
			print row[0]
			print 'FOUND AN OAK!'
			print " "
			csvwrite.writerow([row[0], row[1]])
               
    return 0
    
if (__name__ == "__main__"):
    status = main(sys.argv)

	
doctest.testmod()

sys.exit(status)
