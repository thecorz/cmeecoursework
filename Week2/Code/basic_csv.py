#!/usr/bin/python
"""
How to open, read, manipulate and write csv files
csv.reader
csv.writer

"""
import csv

f = open('../Sandbox/testcsv.csv','rb')
csvread = csv.reader(f)
temp = []
for row in csvread:
	temp.append(tuple(row))
	print row
	print "The species is", row[0]
f.close()

# write a file containing only species name and body mass

f = open('../Sandbox/testcsv.csv', 'rb')
g = open('../Sandbox/bodymass.csv','wb')

csvread = csv.reader(f)
csvwriter = csv.writer(g)
for row in csvread:
	print row
	csvwriter.writerow([row[0], row[4]])
	
f.close()
g.close()



