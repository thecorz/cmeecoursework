"""
This little script shows some aspects of the function of 
sys.argv

"""


import sys
print "This is the name of the script:", sys.argv[0]# "the first argument of sys.argv
							#the file name
print "Number of arguments: ", len(sys.argv) #how many arguments
print "The arguments are: " , str(sys.argv) # which are the arguments
