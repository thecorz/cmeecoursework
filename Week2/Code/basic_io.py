"""
How to open, read and write files in python

How to store thing using pickle

"""

###################
# File input
###################

#f = open ('../Sandbox/test.txt', 'r')
#for line in f:
	#print line,
#f.close()

#f = open ('../Sandbox/test.txt', 'r')
#for line in f:
		#if len(line.strip()) > 0:
			#print line,
			
#f.close()

######################
# FILE OUTPUT
#######################

# Save the elements of a list to a file
list_to_save = ['Javier', 'Cuadrado', 'Corz']

f = open('../Sandbox/testout.txt','w')
for i in list_to_save:
		f.write(str((i) + '\n')) # Add a new line at the end
		
#############################		
# STORING OBJECTS		
############################		
# To save an object (even complex) for a later use
my_dictionary = {"a key": 10, "another key": 11}

import pickle

f = open('../Sandbox/testp.p','wb') ## note the b: accept binary files
pickle.dump(my_dictionary, f) # Pickle takes the dictionary and puts it down
					# in a file (testp). stores it
f.close()

## Load the data again
f = open('../Sandbox/testp.p','rb')
another_dictionary = pickle.load(f) # now pickle takes the file that we create to
			# to store things and uses it to recover the dictionary that we had
f.close()

print another_dictionary
