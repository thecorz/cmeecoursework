"""Some exercices to exemplify the use of control flow functions"""
import sys
#How many times  will 'hello' be printed?

for i in range(3, 17): # prints 14 hellos because there is 14 numbers between 3 and 17
	print 'hello'


for j in range(12): # if the division of j by 3 has a remainder equal to 0
	if j % 3 == 0:
			print 'hello'

for j in range(15): 	
	if j % 5 == 3:		#if the division of j by 5 has a remainder containing 3
		print 'hello'
	elif j % 4 == 3:	#if the division of j by 4 has a remainder containing 3
		print 'hello'	
		
#4)

z = 0 #Sets the variable z equals 0
while z !=15:	#keep the loop running if z is different from 15
	print 'hello'
	z = z + 3  # Adds 3 to z every loop
	
#5)

z = 12
while z < 100: #if z is smaller than 100
	if z == 31: #if z equals 31.
		for k in range(7): #Runs the loop 7 times = prints 7 times
			print 'hello'
	elif z == 18: # if z is 18
		print 'hello'
	z = z + 1 # Adds 1 to z 
	
# What does fooXX do?
def fool(x): 
	""" power the value to the 0.5"""
	return x ** 0.5 # ** is the operator for power. power to the 0.5 is a square rooting
	
def foo2(x, y): 
	"""returns whichever number is bigger out of the two"""
	if x > y:
		return x
	return y

def foo3(x, y, z): 
	"""order the numbers from the smallest to the largest"""
	if x > y:
		tmp = y #If x is bigger than y, we create a temporary variable tmp to name y
		y = x #move x to the y position because x is larger
		x =tmp #move y (we name it tmp temporarily to change its position) to x position
	if y > z:
		tmp = z
		z = y
		y = tmp
	return [x, y, z] # This the order of the variables and the function asaign different
					# names to the numbers according to who is larger
	
def foo4(x):
	"""x factorial. multiply i by the previous number every cycle of the loop""" 
	result = 1
	for i in range(1, x + 1):
		result = result * i
	return result

# This is a recursive function, meaning that the function calls itself
# read about it at
# en.wikipedia.org/wiki/Recursion_(computer_science)

def foo5(x):
	"""x! x factorial. multiply x for the previous number but in a different way than foo4."""
	if x == 1:
		return 1
	return x * foo5(x - 1) #multiply x for the function of the previous number...

foo5(10)

def main(argv): 
	
	#print fool(3)
	#print foo2(4, 9)
	#print foo3(1, 5, 88)
	print foo4(6)
	print foo5(2)
	
if (__name__ == "__main__"):
	status = main(sys.argv)
	sys.exit(status)
	
