#!/usr/bin/python
"""
There is a tuple of tuples with scientific names, common names and weight
of few birds.

The code print the different terms of the tuples in different lines

"""


birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
        )

# Birds is a tuple of tuples of length three: latin name, common name, mass.
# write a (short) script to print these on a separate line for each species
# Hints: use the "print" command! You can use list comprehension!

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

for each_bird in birds: # For each tuple ...
	print (each_bird[0]) + '\n' + (each_bird[1]) + '\n' + str(each_bird[2]) + '\n' # print each term of the tuple and change line

#Trying using list comprehension without success
#[each_bird for each_bird in birds] print (str(each_bird[0]) + '\n' + str(each_bird[1]) + '\n' + str(each_bird[2]) + '\n')
