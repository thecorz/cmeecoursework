"""
How local and global variables work

"""

## Try this first
#The a_global outside of the function is different of the one inside
_a_global = 10

def a_function():
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the value is ", _a_local
	return None
	
a_function()
print "Outside the function, the value is ", _a_global

## Now try this


_a_global = 10

def a_function():
	global _a_global # calls the global variable into the function
	_a_global = 5 # changes the global variable from 10 to 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the value is ", _a_local
	return None
	
a_function()

print "Outside the function, the value is ", _a_global
