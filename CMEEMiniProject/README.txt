TREE HOLES MINIPROJECT

5 folders 

folder SCRIPTS

Data_wrangling.R
Process the data, cleans it and order it ready to be analysed
	Inputs
	Outputs

Data_exploration.R
Do a general exploration of the data, plot a correlogram
	Inputs
	Ouputs

AnalyseB.R
Fit the models and output data to files and creates graphs
	Inputs
	Output

folder DATA
	Folder Raw_data
		DF.EcoDF.csv: abiotic data
		FD_OTU_clean_genus.csv
		Fields_samples.list
	Processed_data
		Abiotic.csv No splited processed data
		Abundancies.csv	No splited processed data
		Rain_Abiotic.csv
		Rain_Abundancies.csv
		TreeHoles_Abiotic.csv
		TreeHoles_Abundancies.csv
folder RESULTS
Contains csv files with outputs of the models coefficients, significance values and dispersion values

forder Manuscripts
Contains photos the latex file and the the script that compile the pdf

folder FIGURES
Contains figures the output of the analysisB.R

P.D. Sorry for the slightly late submission... I was working at the spanish time ;-)

