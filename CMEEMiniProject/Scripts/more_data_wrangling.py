#!/usr/bin/python

""" Data wrangling of abundancies """

import csv
import numpy as np
import scipy as sc
import pandas as pd

f = open('../Data/Processed_data/TreeHoles_Abundancies.csv', 'r')
abund = sc.genfromtxt(f, delimiter = ',', dtype = None)

for i in range(1,406):
	for j in range(1,5160):
		abund[i][j] = (int(abund[i][j]) + 1)

# dimensions of the matrix
abund.shape
abund[400,]

f2 = open('../Data/Processed_data/TreeHoles_Abundancies.csv', 'w')
np.savetxt(abund)
f2.close


#g = open('../Data/Processed_data/TreeHoles_Abiotic.csv', 'r')
#abi = sc.genfromtxt(g, delimiter = ',', dtype = None, names = True)
abi = pd.read_csv('../Data/Processed_data/TreeHoles_Abiotic.csv', sep = ',')

abundInt = np.array(abund[1:,1:], dtype=int)
TotalReadsN = np.sum(abundInt, axis = 1)
abi['TotalReadsN'] = TotalReadsN

abi[:5]
abund[:5]
TotalReadsN = np.array(TotalReadsN, dtype= None)
TotalReadsN = np.insert(TotalReadsN,0,0)	 
TotalReadsN[0] = "TotalReadsN"
abiSum = np.hstack((abi, TotalReadsN))

TotalReadsN.shape
abi.shape
len(TotalReadsN)
type(TotalReadsN)
type(abi)
abund[1][1]
abund = abund.matrix
abund = abund.astype(np.int)
TotalReadsN.dtype.fields = TotalReadsN
abund = abund + 1

TotalReadsN = {'names':['TotalReadsN']}
arr = []
for line in f.readlines():
	arr.append([])
	for i in line.split():
		arr[-1].append(i)

len(abund)
abund[1][1]
F.D.DNA.P1.A3.A.01.ID3.3.15.THS.2.16S.3
F.D.DNA.P1.A3.A.01.ID3.3.15.THS.2.16S.3
F.D.DNA.P1.A3.A.02.ID3.3.15.THS.10.16S.3
