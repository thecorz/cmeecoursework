#!/bin/bash
# This script runs the workflow of the Tree Holes Miniproject

# First set some working directories
mkdir ../Data/Processed_data
mkdir ../Figures
mkdir ../Manuscript
mkdir ../Results

# Run the data wrangling in R
Rscript --verbose Data_wrangling.R

# Run the data wrangling in python

