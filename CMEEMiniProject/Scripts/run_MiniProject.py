#!/usr/bin/python
"""This script runs the workflow of the Tree Holes Miniproject"""


import subprocess
import os.path
subprocess.Popen("Rscript --verbose Data_wrangling.R ", shell=True).wait()
subprocess.Popen("Rscript --verbose Data_exploration", shell=True).wait()
subprocess.Popen("Rscript --verbose AnalysisB.R", shell=True).wait()
subprocess.os.chdir("../Manuscripts")
subprocess.os.system("bash CompileLaTeX.sh")
#~ subprocess.Popen(["Bash", "../Manuscripts/CompileLaTeX.sh"])#.wait()

# First set some working directories


