%% LyX 2.2.1 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{geometry}
\geometry{verbose,tmargin=2cm,bmargin=2.5cm,lmargin=2.3cm,rmargin=2cm}
\usepackage{textcomp}
\usepackage{amsbsy}
\usepackage{graphicx}
%\usepackage[english]{babel}
\usepackage{lineno}
\usepackage{setspace}
\begin{document}

\begin{titlepage}

\title{Selecting the best model relating microbial abundance and environmental factors}
\author{Javier Cuadrado Corz}
\maketitle
\linenumbers
\doublespacing
wordcount 3568

\end{titlepage}
\section{Introduction}

Understanding the ecological and evolutionary determinants shaping
bacterial communities is an urgent task, given the importance of microbial
communities in processes such as environmental regulation or human
health is nowadays recognized \cite{falkowski_biogeochemical_1998,ley_worlds_2008}.
However, we have only started to understand the complexity of the
underlying ecological and evolutionary mechanisms governing assembly
and functioning. One of the most important challenges is to determine
the relative importance of habitat filtering with respect to other
mechanisms, such as neutral assembly. This means that it is necessary
to understand the relationship between environmental variables and
community composition. 

The most frequent approach to the problem consists in classifying
samples according with their similarity in the composition, and then
trying to relate the clusters obtained with environmental variables.
This approximation has been shown to shed some light for instance
on the effect that different treatments (e.g. diet or disease) have
on human gut composition \cite{arumugam_enterotypes_2011}. More direct
approximations like linear or logistic regression models have been
used \cite{faust_cooccurrences_2012}. Nevertheless, under these models
rely assumptions on variance homogeneity and normality that are not
fulfilled in metagenomic microbial data. The reason is that these
data consists of counts with high dispersion and a large number of
zeros, contrary to these assumptions, and transformations of these
data into continuous variables may generate biases \cite{friedman_SparCC_2012i}.

Recently, it has been proposed the use of generalized linear models
to fit microbial abundances to covariates \cite{xu_ZINBmicrob_2015}.
Working with simulated data, in this work the author show the effect
that a single binary treatment has in predicting the artificially
generated abundances, for a variety of generalized linear models.
My objective in this work is to reproduce these computational experiments
with real data, and considering a large amount of environmental variables,
both categorical and numerical. Therefore, I aim to understand which
kind of generalized linear model better fits a set of environmental
variables to microbial abundances. In the last section, I will discuss
how these results will be helpful to investigate the relationship
between environmental variables and community composition.

\subsection*{Generalized linear models (GLMs)}

The response variable I aim to model is a vector of counts $\boldsymbol{Y}$
of a given Operational Taxonomic Unit (OTU, see Methods) observed
in different samples $Y_{1},...,Y_{M}$, being $M$ the total number
of samples. Being counts, the first natural assumption is that the
variable follows a Poisson distribution $Y_{i}\sim\mathrm{Poiss}(\lambda_{i})$,
where $\lambda_{i}$ is the mean of the distribution. The idea is
to perform a regression of the covariates, in our case environmental
variables $X_{ik}=X_{i1},...,X_{iS}$ (with $S$ the number of total
variables), on the parameters of the distribution through the GLM
canonical link:

\begin{equation}
\log(\lambda_{i})=\gamma_{0}+\log(T_{i})+\sum_{k}\gamma_{k}X_{ik}.\label{eq:PoissLogLink}
\end{equation}

The variable $T_{i}$ is the number of total counts observed in the
sample, which we introduce as an offset. In this way, we account for
differences in the number of reads of the samples, because we are modelling
in the above expression the relative abundance as $\log(\lambda_{i})-\log(T_{i})=\log(\lambda_{i}/T_{i})$.
A similar procedure is used for the negative binomial, which considers
an extra dispersion parameter. 

\subsection{Zero Inflated Models ZI}
As I anticipated, however, these data, contain a large number of zeros. To account for the excess of zeros,
it is possible to assume that there is an additional stochastic variable
generating these zeros, which I model with a Bernouilli trial. With
some probability $\pi$ there is a zero count observation and, with
probability $1-\pi$ and observation that follows the chosen distribution.
This leads to a new kind of zero inflated generative models \cite{lambert_ZIP_1992}.
For instance, the Zero Inflated Poisson model (ZIP) would be defined
as:

\begin{eqnarray*}
Y_{i} & \sim & 0\;{\textstyle with\ probability}\,\pi_{i}\\
 & \sim & \mathrm{Poiss}(\lambda_{i})\;{\textstyle with\ probability}\,1-\pi_{i}
\end{eqnarray*}

with a similar expression for the Zero Inflated Negative Binomial
(ZINB), just changing the $\mathrm{Poiss}$ by the $\mathrm{NB}$
distribution. To estimate the probability $\pi_{i}$ we perform as
well a GLM with the canonical logit function:

\[
\log\left(\frac{\pi_{i}}{1-\pi_{i}}\right)=\beta_{0}+\sum_{k}\beta_{k}X_{ik}
\]

where we should note that it is not introduced now the offset, because
it is considered a fixed value that affects the amount of reads observed
rather than a stochastic variable affecting both structural zeros
and observed abundances. In summary, in this work I will test the
performance of Poisson (P), Negative Binomial (NB), ZIP and ZINB for
a large set of OTUs and I will determine which are the conditions
influencting the selection of one over other model.

\section{Methods}

\subsection{Sampling procedure}

Cavities in trees have been proved to be a good system to study biodiversity in microbial communities as opposite to experimental designs where the microbial ecosystem of study was a single contiguous habitat. Water-filled treeholes function as islands for microbes. Each of these islands contain a micro-ecosystem that obtain its nutrients from leave litter.  
All samples were taken from a single European Beech tree (Fagus sylvatica) located in the grounds of Silwood
Park (+51 24' 29.52", -0 38' 42.72"). A number of treeholes were identified within the root system, of which
ten were deemed suitable (i.e. not connected, of a volume sufficient for water and detritus) for inclusion in
the survey. Samples were collected every four weeks from November 2014 to November 2015. Extra samples
were collected on a weekly basis during the months of January (winter), April (spring), July (summer) and
October (autumn) to allow for detection of short term community dynamics. From each treehole, samples were
collected from both the aqueous (if possible) and sediment phases. Soil and rainwater samples were collected from compass points around the base of the tree at a distance of 1.5m thus providing information about the local and invading populations respectively. Approximately 5 g of detritus or soil, and 5 ml of water was collected at each sampling point. Detritus and soil samples were washed by adding a 1 ml phosphate buffered saline (PBS) at pH 7.0 (Sigma-Aldrich, Gillingham, UK) per 1 g sample, vigorously mixing and leaving to settle for ~20 minutes. Liquid (including wash samples) and neat solid samples were frozen for molecular analysis at -80°C with paired samples frozen in 30\% v/v glycerol (Sigma-Aldrich) with 0.85\% w/v sodium chloride (Sigma-Aldrich). Average temperature and rainfall data were obtained from the Silwood Park weather monitoring station for each week over the period from which the samples were taken.




\subsection{Sequencing}

NA and RNA were extracted simultaneously from the samples using the protocol previously described by Griffiths et al., [date].  All reagents were treated with DiEthyl PyroCarbonate (DEPC) prior use to inactivate RNase enzymes. The extracted nucleic acids were suspended in sterile DEPC-treated water and divided into two 25  l aliquots. One aliquot was stored at -20C and the other treated with DNase 1 (Thermo Scientific) following manufacturer's instructions to remove DNA from the sample leaving only RNA. Fragments of cDNA were obtained from a reverse-transcription of a subsample of the latter, using RNA-to-cDNA kit (Applied Biociences) following manufacturer's instructions and stored at -20C until use.

Sequencing was conducted according to the Schloss Protocol 16S rRNA Illumina MiSeq V3 paired end reads (2x300bp). Sequenced paired-end reads were joined using PEAR (Zhang et al. 2014), quality filtered using FASTX tools (Hannon, http://hannonlab.) and chimeras were identified and removed with VSEARCH\_UCHIME\_REF (Rognes, https://github.com/) using Greengenes Release 13\_5 (97\%) (DeSantis et al., 2006). Singletons were removed and the resulting sequences were clustered into operational taxonomic units (OTUs) with VSEARCH\_CLUSTER\_FAST at 97\% sequence identity (Tindall et al 2010). Representative sequences for each OTU were taxonomically assigned by RDP Classifier (Wang et al. 2007) using the Greengenes Release 13\_5 (full) (DeSantis et al., 2006) as the reference. The sequences of the hypervariable regions of bacterial 16S rRNA gene were clustered into operational taxonomic units (OTU)

\subsection{Covariates selection}

A total of 18 abiotic variables were recorded during the sampling period, among them, environmental variables were the most numerous: minimum, maximum and mean Air Temperature ; minimum, maximum and mean Grass Temperature; 2 and 4 Inch Soil Temperature, Rainfall and number of days of drought. The position of each treehole relative to the North was recorded as well as their shape, which is thought to be related to migration, e.g. invasion rate, which could be related to biodiversity levels. The shape of each treehole was characterized by measuring its depth and surface area. The volume was calculated from the these two, and it has been directly related to biodiversity levels in microbial communities. Wetness of treeholes was measured through three different variables. The water content of the collected samples was obtained and it was used to define a variable that determine whether the substrate of the treehole was saturated with water or not. At the same time if each treehole was filled with water or not was taken note of. 
Some of the variables were discarded because were not relevant for the purpose of this study (e.g. Distance from previous tree hole), were incomplete (e.g. number of days of drought) or were redundant. As it was expected all of the environmental Temperature variables were highly correlated with each other. (fig. \ref{fig:TcorTest}). In the beginning few of these temperature variables were intended to be part of the models because of their a priory biological importance. But because of the high sensitivity to the presence of collinear explanatory variables of some of the competing models, eventually only one temperature variable was selected, Mean Air Temperature, over the others. Minimum and Maximum Temperatures, despite of presumably playing a very important role in determining the diversity of microbial communities as measure of extreme events the community have been through, information about the frequency of this events was not available. Therefore, having only one  measurement to describe the Temperature of the system between each sample, Mean Air Temperature was considered more informative at this respect. For the sake of simplicity, the derived categorical variable that stated whether the substrate of the treehole was saturated with water or not was considered similar to the observed categorical variable that informed if the treehole was flooded or not. The latter was considered more informative as it was directly observed while the former was derive from the water content of the sample. 
As it has been mentioned before, samples were collected at different time points. This constitutes a challenge when the object of the study is to determine how abiotic factors influence microbial biodiversity abundance, because to include the time dimension in the model adds a whole new level of complexity to the system.

\begin{table}
\centering
\begin{tabular}{ |p{3cm}|p{3cm}|p{6cm}|  }
   
 \hline
 
 Tree Hole   		& Categorical	& Tree Hole ID of origin of the sample \\
 Date				& Categorical	& Date when the sample was taken 	\\
 Water saturation	& Binary		& Soil saturated with water   		\\
 Flooded			& Binary		& Hole flooded						\\
 Surface Area	    & Continuous	& Area occupied by the tree hole	\\
 Depth				& Continuous	& Depth of the tree hole 			\\
 Degrees North		& Continuous	& Orientation of the hole from the North \\
 Air Temperature	& Continuous	& Mean Temperature 					\\
 Rainfall			& Continuous	& Mean precipitation				\\
 Total Abundance	& Continuous	& Sum of all the Abundances of all the OTU's present in the sample \\	
 \hline

\end{tabular}
\caption{Explanatory variables}
\label{tab:Factors}
\end{table}

 But before considering using time series regression models we have to examine carefully in which scale the samples are related in time. On the one hand, some of the time intervals between sampling are long enough to consider that the microbial communities of two consecutive samples are too far related in time to each other so that it is impossible to distinguish if these two samples were drown from the same community in two different times or were drown from two different independent communities. Furthermore, taking into account the very short generation rate and the rate of immigration in microbial communities one month or even one week is too long to capture the mechanisms underlying microbial community dynamics.
 On the other hand, from unpublished work carried out in the laboratory, it has been observed that samples that have been collected even from different trees in different time points can be fitted to neutral models considering just one single metacommunity. Accordingly, we could consider that these samples belonging to different time points can be considered as different realizations of the same metacommunity, not differing from samples of different holes. Nevertheless, we introduce an additional factor variable in the models 
 that groups all the samples belonging to the same time point. In so doing we expect it will account for any significant effect in the microbial diversity abundance.


\begin{figure}[htp]
\centering
\includegraphics[scale=0.9]{../Figures/TcorTest.pdf}
\caption{Correlation among environmental temperature variables. As it was expected all temperature variables are highly correlated. Only Mean Air Temperature was selected among them to be included in the competing models}
\label{fig:TcorTest}
\end{figure}

\subsection{OTUs selection}

The advent of nest generation sequencing technology has open a whole new world of possibilities that allow researchers to identify and quantify a lot of microbial diversity present in a sample without any kind of bias produced by the choice of culture media, as it was used the be the norm before the development of this tools. All this new powers need to be harnessed as the increase in the detection of microbes has produced an unprecedented amount of metagenomic data easily available. But no all the metagenomic data produced in a study is relevant. It is mandatory to plan and think which of the available data is relevant for the purpose of the study. Hence it is always necessary to follow a process of data selection and data cleansing.  

34.442 OTU's were found among all the samples collected in this study. Abundances lower than 5 individuals per sample were discarded as methodological error. A minimal sequencing effort of 10.000 reads per sample was required to get the sample included in the study. OTU's that were present in less than 10 samples were discarded because of its lesser relative importance in the microbial communities studied. After data cleansing, we obtained 3.728 OTU's from which a randomly chosen set of 500 was drawn. It has been evaluated that a communty size of 500 OTU's is big enough to show the performance of the different competing models without compromising to much computing time

\subsection{Model competition}

I performed regressions for the resultant set of OTUs, using the packages glm for Poisson and Negative Binomial models and pscl for zero inflated and Hurdle models in R [Refs].

 As we deal with nested and non-nested models, only some particular kind  of tests to determine the best model can be used. AIC test was selected as programmed in R packages [Ref]. For each OTU, I performed all four models (P, NB, ZIP and ZINB) and I made an all-against-all test to determine which of the models performs better. The model selected is classified, together with three properties of the OTU, namely the number of zeros, its total abundance, and the mean and standard deviation of the OTU's abundance among the samples.
 All the coefficients and its significance (P-values) are recovered and stored for future analysis.

\section{Results}

From the AIC values it can be remarkably conclude that Zero Inflated Negative Binomial is the model that fit the best abundance data of microbiological communities. ZINB was the model that best fit the abundance data in over 60 \% of the OTU's. It is followed by Zero Inflated Poisson that was the best in nearly 20 \% of the OTU's, and Hurdle Negative Binomial in 13 \% of the OTU's. By contrast did not perform very well in comparison with the two parts models. It is noticeable the poor performance of the Hurdle Poisson model which was the best model for only one OTU out of 500.

There are a number of important differences in how the different models perform according to some of the descriptors of the data. In regard of the performance of models in relation to the total abundance of the OTU's across all the samples, the results are consistent with the overall performance results, where the figure  \ref{fig:BestModel_abund} shows how ZINB is consistently the best model across all the range of abundances observed in the data, being the best model for 60 \% of the OTU's which is up to  three times the number of OTU's in which the second placed, ZIP model, is the best. This is results are consonant with the general results we have already  Consistenly with the overall results, 
\begin{figure}
	\includegraphics[scale = 0.9]{../Figures/BestModel_abund.pdf}
	\caption{\textbf{Performance of the competing models in relation to the total abundance of the each OTU across all the samples. } The OTU's were homogeneously distributed among all the classes, 100 OTU's per class. The boundaries of the classes are 1. 60 to 157 reads; 2. from 163 to 328 reads; 3. from 333 to 678 reads; 4. from 678 to 1828 reads; 5. form 1833 to 68679}
	\label{fig:BestModel_abund}
\end{figure}

The most striking result of this study can be seen on the graph \ref{fig:BestModel_zeros}. It shows how ZINB models completely outperform the rest of the models for OTU's with low and medium number 0 abundances. But at the same time it can be seen how its performance decreases with an increase in the presence of 0 in the data, while at the same time ZIP models don't give good responses with low levels of 0, so much so that it is no even present in the group of OTU's with the lowest presence of 0 but it gradually increases its performance to such an extent that finally outperforms the rest of the models in the group of OTU's with the largest number 0 abundances, being the best model in over half of the OTU's in this group.


  
\begin{figure}
	\includegraphics[scale = 0.9]{../Figures/BestModel_zeros.pdf}
	\caption{Performance of the competing models in relation to the number of 0 abundances present in the data.}
	\label{fig:BestModel_zeros}
\end{figure}

From the figure we can observe that ZIP models are the best for fitting abundances that are not very disperse. ZIP is best model in a 70 \% of the OTU's that have a low level of dispersion. This is consistent with the fact that Poisson models assume that the data are homogeneously disperse. Poisson is a special case of Negative Binomial for when the dispersion parameter is 1. Poisson models are nested within Negative Binomial. The graph shows perfectly how ZINB are better when the level of dispersion of the data increases, as it can model overdispersed data while the performance of ZIP decreases as the dispersion increases.  

\begin{figure}
	\includegraphics[scale = 0.9]{../Figures/BestModel_SD.pdf}
	\caption[Performance of the compiting models in relation to the dispersion of the abundances of each OTU measured as Standard Desviation]{ The OTU's were homogeneously distributed among all the classes, 100 OTU's per class. The boundaries of the classes are  1. 1.31 to 3.02; 2. from 3.04 to 5.39; 3. from 5.42 to 9.30; 4. from 9.35 to 20.66 ; 5. form 21.04 to 366.51}
\end{figure}




\section{Conclusions and further work}

The results of the study what it was expected: the performance of Zero Inflated models is far better when modelling Zero Inflated data than classical model e.g. Poisson and Negative Binomial regression. If this study was to be part of a bigger study the model of choice would be clearly ZINB, This models are very robust because they can cope with over dispersed data .  I 

Surprisingly hurdle models did not perform as well as it was expected. Hurdle models are considered to be as good as ZI to cope with zero inflated data. 

During the model selection process for this study numerous problems have been encountered. As it has mentioned before in methodology, the fact that samples were collected in different time points implies that the time dimension could be taken into account in the model. In the beginning it was thought to include time as a categorical factor, but that involved a categorical factor with 23 levels. Identification of treehole was as well intended to be included in the model. But as in the case of treating time as a categorical factor, the tested models did not cope well with categorical factor with many levels. It was considered to used Generalised Linear Mixed Models including time points and tree hole ID as random effects but the complexity of the model and the difficulty of its interpretation discouraged its use for a study of this nature. If the scope of this study was wider there is not doubt that the models of choice would be to explore Zero Inflated and Hurdle Models with mixed effects (there are already some packages in R) which open the door to explore different and new exciting ways of explaining the change of abundances in different time points. 
Other possible ways to take this study further would be to explore the possibility of using time series regression models, which were again, out of the scope of this study but open whole new way of modelling the dynamics of the microbial community. 

This study show the first step of what could be more ambitious study which once the significant coefficients are obtained, it allow us to classify the OTU's according to their covariation (also called "consonant effects) which would indicate similarities in environmental preference of the OTU's.   


\section*{Acknowledgements}

The author would like to acknowledge Dr. Damian Rivett and Mrs. Shorok Mombrikotb who carried out the collection, process and sequencing of the samples. This study was hosted in the laboratory of Prof. Thomas Bell under the invaluable help and supervision of Dr. Alberto
Pascual-García who was willing to answer all my question and soften all the hardship of this study

\bibliographystyle{plain}
\bibliography{TreeHoles}

\end{document}
