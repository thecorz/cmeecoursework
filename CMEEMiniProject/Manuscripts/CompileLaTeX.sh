#!/bin/bash
pdflatex TreeHoles_Miniproject.tex
pdflatex TreeHoles_Miniproject.tex
bibtex TreeHoles_Miniproject
pdflatex TreeHoles_Miniproject.tex
pdflatex TreeHoles_Miniproject.tex
#~ evince $1.pdf &

## Cleanup
rm *~
rm *.aux
rm *.dvi
rm *.log
rm *.nav
rm *.out
rm *.snm
rm *.toc
